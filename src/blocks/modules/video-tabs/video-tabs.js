import "jquery.nicescroll"
import "bootstrap/js/src/tab.js"

const videoTabSelector = $("#video-tab");

videoTabSelector.niceScroll({
  cursorcolor:"#B1B1B1",
  cursorborder:'none'
});
