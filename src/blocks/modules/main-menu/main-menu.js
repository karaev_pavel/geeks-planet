if (window.innerWidth < 992) {
  $('.nav-btn').click(function() {
    $(this).toggleClass('close');
  })
  $('.main-menu .has-dropdown > a').click(function(e) {
    e.preventDefault()
		$(this).parent().toggleClass('open');
	});
} else {
  $('.main-menu .has-dropdown').hover(function() {
    $(this).addClass('open');
  }, function() {
    $(this).removeClass('open');
  });
}

$(window).resize(function() {
  if (window.innerWidth < 992) {
    $('.nav-btn').click(function() {
      $(this).toggleClass('close');
    })
    $('.main-menu .has-dropdown > a').click(function(e) {
      e.preventDefault()
      $(this).parent().toggleClass('open');
    });
  } else {
    $('.main-menu .has-dropdown').hover(function() {
      $(this).addClass('open');
    }, function() {
      $(this).removeClass('open');
    });
  }  
})


