let dropdownProfile = $('.user-dropdown');
let dropdownProfileMenu = $('.dropdown-menu');

let searchBtnOpen = $('.btn-search');
let headerSearch = $('.header-search');
let searchBtnClose = $('.search-close');

dropdownProfile.click(function(){
  dropdownProfileMenu.toggleClass('show');
});

$(document).mouseup(function (e) {
   if (dropdownProfile.has(e.target).length === 0){
    dropdownProfileMenu.removeClass('show');
   }
})

searchBtnOpen.click(function(){
  headerSearch.toggleClass('show');
});

searchBtnClose.click(function(){
  headerSearch.removeClass('show');
});