import 'owl.carousel';

const mainCarousel = $('.slider');
const nextBtn = $('.slider-next');
const prevBtn = $('.slider-prev');

mainCarousel.owlCarousel({
  loop: true,
  nav: false,
  dots: false,
  lazyLoad: true,
  responsive: {
    0:{
      items:1,
    },
    992:{
      items:3,
    }
  }
});

nextBtn.click(function() {
  mainCarousel.trigger('next.owl.carousel');
});

prevBtn.click(function() {
  mainCarousel.trigger('prev.owl.carousel');
});