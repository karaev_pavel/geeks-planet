$('.owl-posts').owlCarousel({
  margin: 5,
  loop: true,
  dots: false,
  autoplay: false,
  responsive: {
    0: {
      items: 1
    },
    1024: {
      items: 1,
      center: false
    },
    1200: {
      items: 2,
      center: true
    }
  }
});